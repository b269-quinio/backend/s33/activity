

fetch ("https://jsonplaceholder.typicode.com/todos/", {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => {
	let titles = json.map(item => item.title)
	console.log(titles);
});



// GET Method
fetch ("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json))
.then((json) => console.log(`The item "delectus aut autem" on the list has a status of false`));



// POST Method
fetch ("https://jsonplaceholder.typicode.com/todos/", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 1,
		title: "Created To Do List Item",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// PUT Method
fetch ("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		dateCompleted: "Pending",
		status: "Pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH Method
fetch ("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "delectus aut autem",
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// DELETE Method
fetch ("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'DELETE'
})
.then((response) => response.json())
.then((json) => console.log(json));